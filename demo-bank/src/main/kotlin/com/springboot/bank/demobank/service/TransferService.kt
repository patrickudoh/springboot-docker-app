package com.springboot.bank.demobank.service

import com.springboot.bank.demobank.entity.Transfer
import com.springboot.bank.demobank.exception.TransferNotFoundException
import com.springboot.bank.demobank.repository.TransferRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class TransferService(
    private val transferRepository: TransferRepository
) {
    fun createTransfer(transfer: Transfer): Transfer {
       return  transferRepository.save(transfer)
    }

    fun getAllTransfers(): MutableList<Transfer> {
       return transferRepository.findAll()
    }

    fun getTransferById(id:String): Transfer {
       return  transferRepository.findById(id).orElseThrow{ TransferNotFoundException(HttpStatus.NOT_FOUND, "No matching Transfer was found!") }
    }

    fun updateTransferById(transfer: Transfer, id:String): Transfer {
       return if(transferRepository.existsById(id)){
           transferRepository.save(
               Transfer(
                   id = transfer.id,
                   targetAccount =  transfer.targetAccount,
                   value =  transfer.value,
                   description = transfer.description,
               )
           )
       }else throw TransferNotFoundException(HttpStatus.NOT_FOUND, "No matching Transfer was found!")


    }

    fun deleteTransferById(id:String){
        return if(transferRepository.existsById(id)){
            transferRepository.deleteById(id)
        }else throw TransferNotFoundException(HttpStatus.NOT_FOUND, "No matching Employee was found!")
    }
}