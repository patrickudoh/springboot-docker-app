package com.springboot.bank.demobank.resource

class TransferResourceWithId(
    val id: String,
    val targetAccount: String,
    val value: Double,
    val description: String = "",
) {
}