package com.springboot.bank.demobank.exception

import org.springframework.http.HttpStatus

class TransferNotFoundException(notFound: HttpStatus, message: String) : Throwable() {

}
