package com.springboot.bank.demobank.entity

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Transfer(
    @Id
    val id: String,
    val targetAccount: String,
    val value: Double,
    val description: String
) {

}