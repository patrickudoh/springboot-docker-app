package com.springboot.bank.demobank.repository

import com.springboot.bank.demobank.entity.Transfer
import org.springframework.data.jpa.repository.JpaRepository

interface TransferRepository: JpaRepository<Transfer, String> {
}