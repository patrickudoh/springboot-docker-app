package com.springboot.bank.demobank.resource

class TransferResource(
    val targetAccount: String,
    val value: Double,
    val description: String = "",
) {
}