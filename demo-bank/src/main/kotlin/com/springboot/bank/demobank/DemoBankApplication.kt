package com.springboot.bank.demobank

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoBankApplication

fun main(args: Array<String>) {
	runApplication<DemoBankApplication>(*args)
}
