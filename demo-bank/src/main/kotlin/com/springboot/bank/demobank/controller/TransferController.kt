package com.springboot.bank.demobank.controller

import com.springboot.bank.demobank.entity.Transfer
import com.springboot.bank.demobank.resource.TransferResource
import com.springboot.bank.demobank.resource.TransferResourceWithId
import com.springboot.bank.demobank.service.TransferService
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class TransferController(private  val transferService: TransferService) {

    @PostMapping("/transfers")
    fun createTransfer(@RequestBody transferResource : TransferResource): TransferResourceWithId {
        val createdTransfer = Transfer(
            UUID.randomUUID().toString(),
            transferResource.targetAccount,
            transferResource.value,
            transferResource.description
        )
      val savedTransfer =   transferService.createTransfer(createdTransfer)
        return TransferResourceWithId(savedTransfer.id, savedTransfer.targetAccount, savedTransfer.value, savedTransfer.description)
    }

    @GetMapping("/transfers")
    fun getAllTransfers(): MutableList<Transfer> {
        return transferService.getAllTransfers()
    }

    @GetMapping("/transfers/{id}")
    fun getSingleTransfer(@PathVariable("id") id:String): Transfer {
        return transferService.getTransferById(id)
    }

    @PutMapping("/transfers/{id}")
    fun updateTransfer(transfer: Transfer, @PathVariable("id") id:String): Transfer {
        return transferService.updateTransferById(transfer, id)
    }

    @DeleteMapping("/transfers/{id}")
    fun deleteTransfer(@PathVariable("id") id:String){
        return transferService.deleteTransferById(id)
    }

}